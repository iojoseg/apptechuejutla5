package mx.tecnm.misantla.appith5.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_uno.*
import mx.tecnm.misantla.appith5.R
import mx.tecnm.misantla.appith5.model.Persona

class UnoFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_uno, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_Enviar.setOnClickListener {

            val nombre = edt_Name.text.toString()

            val persona = Persona(nombre,"Gutierrez")

            val direction = UnoFragmentDirections.actionUnoFragmentToDosFragment(persona)
            Navigation.findNavController(it).navigate(direction)
        }
    }

}